# Combat PLugin for Valence

This project was a attempt to implement the minecraft vanilla combat system in [valence](https://github.com/valence-rs/valence);
Iam not actively developing it for now;

All the code for the melee stuff works pretty much; the ranged weapons like bow, snowballs, or trident i have not started on yet;

The actual plugin code is exposed via the lib.rs, there is also a bin file that serves as demonstration;
You can just `cargo run` it to see the demonstration;
