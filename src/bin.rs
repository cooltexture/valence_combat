use combat_plugin::{CombatPlugin, CombatState, PlayerHitEvent, PlayerKilledEvent};
use tracing::Level;
use valence::{
    entity::{
        entity::NameVisible,
        living::{Health, LivingEntity},
        player::PlayerEntityBundle,
        zombie::ZombieEntityBundle,
    },
    log::LogPlugin,
    player_list::{DisplayName, PlayerListEntryBundle},
    prelude::*,
};

const SPAWN_POS: [f64; 3] = [0.5, 121.0, 0.5];

pub fn main() {
    let network = NetworkSettings {
        connection_mode: ConnectionMode::Offline,
        address: "0.0.0.0:25565".parse().unwrap(),
        ..Default::default()
    };

    // tracing_subscriber::fmt().with_max_level(Level::INFO).init();

    let mut app = App::new();
    app.insert_resource(network)
        .add_plugins(DefaultPlugins.set(LogPlugin {
            level: Level::DEBUG,
            ..Default::default()
        }))
        .add_plugins(CombatPlugin)
        .add_systems(Startup, setup)
        .add_systems(
            Update,
            (
                heal_dummys,
                init_clients,
                toggle_gamemode_on_sneak,
                respawn,
                show_hit_info,
            ),
        );

    app.run();
}

fn setup(
    mut commands: Commands,
    dimensions: Res<DimensionTypeRegistry>,
    biomes: Res<BiomeRegistry>,
    server: Res<Server>,
) {
    let mut layers = LayerBundle::new(ident!("overworld"), &dimensions, &biomes, &server);

    for z in -5..5 {
        for x in -5..5 {
            layers.chunk.insert_chunk([x, z], UnloadedChunk::new());
        }
    }
    for z in -200..200 {
        for x in -200..200 {
            for y in 80..100 {
                layers.chunk.set_block([x, y, z], BlockState::DIRT);
            }
            layers.chunk.set_block([x, 100, z], BlockState::GRASS_BLOCK);
        }
    }
    for z in -20..20 {
        for x in -20..20 {
            layers.chunk.set_block([x, 120, z], BlockState::STONE);
        }
    }
    let layer = commands.spawn(layers).id();

    commands.spawn((
        ZombieEntityBundle {
            layer: EntityLayerId(layer),
            position: Position(DVec3::new(0.5, 101.0, 7.5)),
            entity_name_visible: NameVisible(true),
            head_yaw: HeadYaw(-180.5),
            ..Default::default()
        },
        CombatState::default(),
    ));
    let uuid = UniqueId::default();
    commands.spawn(PlayerListEntryBundle {
        uuid,
        username: Username("Hit Me".to_string()),
        display_name: dummy_name(),
        ..Default::default()
    });

    commands.spawn((
        PlayerEntityBundle {
            layer: EntityLayerId(layer),
            position: Position(DVec3::new(5.0, 101.0, 8.0)),
            head_yaw: HeadYaw(-180.5),
            uuid,
            ..Default::default()
        },
        CombatState::default(),
    ));
}

fn dummy_name() -> DisplayName {
    DisplayName(Some(
        "Hit ".into_text().color(Color::WHITE) + "ME".into_text().color(Color::RED).bold(),
    ))
}

fn toggle_gamemode_on_sneak(
    mut clients: Query<&mut GameMode>,
    mut events: EventReader<SneakEvent>,
) {
    for event in events.iter() {
        let Ok(mut mode) = clients.get_mut(event.client) else {
            continue;
        };
        if event.state == SneakState::Start {
            *mode = match *mode {
                GameMode::Survival => GameMode::Creative,
                GameMode::Creative => GameMode::Survival,
                _ => GameMode::Creative,
            };
        }
    }
}

fn init_clients(
    mut clients: Query<
        (
            &mut EntityLayerId,
            &mut VisibleChunkLayer,
            &mut VisibleEntityLayers,
            &mut Position,
            &mut Health,
        ),
        Added<Client>,
    >,
    layers: Query<Entity, (With<ChunkLayer>, With<EntityLayer>)>,
) {
    for (mut layerid, mut c_layer, mut e_layer, mut pos, mut health) in &mut clients {
        let layer = layers.iter().next().unwrap();
        layerid.0 = layer;
        c_layer.0 = layer;
        e_layer.0.insert(layer);
        health.0 = 20.0;
        pos.set(SPAWN_POS);
    }
}

fn respawn(
    mut deaths: EventReader<PlayerKilledEvent>,
    mut players: Query<(&mut Client, &mut Position, &mut Health, &Username)>,
) {
    for death in deaths.iter() {
        if let Ok([mut killed, killer]) = players.get_many_mut([death.killed, death.killer]) {
            killed.1.set(SPAWN_POS);
            killed.2 .0 = 20.0;
            killed.0.send_chat_message(format!(
                "You died to: {}. They had {} health left",
                killer.3, killer.2 .0
            ));
        }
    }
}

fn show_hit_info(
    mut hits: EventReader<PlayerHitEvent>,
    mut entities: Query<Option<&mut Client>, With<CombatState>>,
) {
    for hit in hits.iter() {
        if let Ok([damagee, damager]) = entities.get_many_mut([hit.damagee, hit.damager]) {
            if damagee.is_none() && damager.is_some() {
                if hit.criting {
                    damager
                        .unwrap()
                        .send_chat_message(format!("You dealt {} damage with a crit", hit.damage));
                } else {
                    damager.unwrap().send_chat_message(format!(
                        "You dealt {} damage without a crit",
                        hit.damage
                    ));
                }
            }
        }
    }
}

fn heal_dummys(mut dummys: Query<&mut Health, (With<LivingEntity>, Without<Client>)>) {
    for mut dummy in dummys.iter_mut() {
        dummy.0 = 20.0;
    }
}

// fn display_action_bar(timer: Local<u64>, Query<) {
// maybeTODO display can crit, atack speed, health, weapon damage
// }
