use valence::prelude::*;

#[derive(Event)]
pub struct PlayerKilledEvent {
    pub killer: Entity,
    pub killed: Entity,
}

#[derive(Event)]
pub struct PlayerHitEvent {
    pub damager: Entity,
    pub damagee: Entity,
    pub criting: bool,
    pub damage: f32,
}
