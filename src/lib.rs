use valence::prelude::*;
mod events;
mod melee;
use melee::*;

pub use events::*;

#[derive(Component, Default)]
pub struct CombatState {
    pub last_attacked_tick: i64,
    pub last_tick_attacking: i64,
    pub can_crit: bool,
}

pub struct CombatPlugin;

impl Plugin for CombatPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            PostUpdate,
            (
                handle_combat_events,
                apply_attack_speed,
                combat_sounds.after(handle_combat_events),
            ),
        )
        .add_event::<PlayerKilledEvent>()
        .add_event::<PlayerHitEvent>()
        .add_systems(
            Update,
            (
                detect_crits.before(apply_knockback),
                give_client_state,
                apply_knockback,
                reset_attack_cooldown,
            ),
        );
    }
}

#[cfg(test)]
mod tests {
    // use super::*;
    // TODO write tests

    #[test]
    fn it_works() {
        // do this https://bevy-cheatbook.github.io/patterns/system-tests.html?highlight=Test#writing-tests-for-systems
    }
}
