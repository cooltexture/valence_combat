use std::borrow::Cow;

mod item_values;
use item_values::*;
mod calc_values;

use bevy_ecs::query::WorldQuery;
use valence::entity::living::Health;
use valence::entity::EntityId;
use valence::event_loop::PacketEvent;
use valence::hand_swing::HandSwingEvent;
use valence::inventory::HeldItem;
use valence::math::Vec3Swizzles;
use valence::protocol::encode::WritePacket;
use valence::protocol::packets::play::entity_attributes_s2c::{
    AttributeModifier, AttributeProperty,
};
use valence::protocol::packets::play::{DamageTiltS2c, UpdateSelectedSlotC2s};
use valence::protocol::packets::play::{EntityAttributesS2c, EntityDamageS2c};
use valence::protocol::sound::{Sound, SoundCategory};
use valence::protocol::VarInt;
use valence::{prelude::*, Layer};

use crate::events::{PlayerHitEvent, PlayerKilledEvent};
use crate::CombatState;

pub fn give_client_state(clients: Query<Entity, Added<Client>>, mut commands: Commands) {
    clients.for_each(|c| {
        commands.entity(c).insert(CombatState {
            last_attacked_tick: 0,
            last_tick_attacking: 0,
            can_crit: false,
        });
    });
}

// world query means we can query this struct and get every entity that
// has Client, Position, CombatState and EntityStatuses.
#[derive(WorldQuery)]
#[world_query(mutable)]
pub struct CombatQuery {
    client: Option<&'static mut Client>,
    state: &'static mut CombatState,
    health: &'static mut Health,
    held: Option<&'static HeldItem>,
    inv: Option<&'static Inventory>,
}

pub fn reset_attack_cooldown(
    mut left_clicks: EventReader<HandSwingEvent>,
    mut entities: Query<&mut CombatState>,
    server: Res<Server>,
) {
    for left_click in left_clicks.iter() {
        if let Ok(mut state) = entities.get_mut(left_click.client) {
            state.last_attacked_tick = server.current_tick();
        }
    }
}

pub fn handle_combat_events(
    server: Res<Server>,
    mut clients: Query<CombatQuery>,
    mut interact_entity: EventReader<InteractEntityEvent>,
    mut hit_writer: EventWriter<PlayerHitEvent>,
    mut kill_writer: EventWriter<PlayerKilledEvent>,
) {
    for event in interact_entity.iter() {
        if let Ok([mut attacker, mut victim]) = clients.get_many_mut([event.client, event.entity]) {
            // Victim or attacker does exist, and the attacker is not attacking itself
            if server.current_tick() - victim.state.last_attacked_tick < 10 {
                continue;
            };
            let (damage, criting) =
                calc_values::calc_final_damage(&attacker, server.current_tick());

            if victim.health.0 - damage <= 0.01 {
                kill_writer.send(PlayerKilledEvent {
                    killer: event.client,
                    killed: event.entity,
                })
            } else {
                victim.health.0 -= damage;
                hit_writer.send(PlayerHitEvent {
                    damager: event.client,
                    damagee: event.entity,
                    damage,
                    criting,
                });
            }

            victim.state.last_attacked_tick = server.current_tick();
            attacker.state.last_tick_attacking = server.current_tick();
        }
    }
}

pub fn combat_sounds(
    mut hits: EventReader<PlayerHitEvent>,
    mut chunk_layers: Query<&mut ChunkLayer>,
    players: Query<(&Position, &EntityId)>,
) {
    for hit in hits.iter() {
        if let Ok([(_, attacker_id), (victim_pos, victim_id)]) =
            players.get_many([hit.damager, hit.damagee])
        {
            let sound_effect: Sound = if hit.criting {
                Sound::EntityPlayerAttackCrit
            } else if hit.damage < 4.0 {
                Sound::EntityPlayerAttackWeak
            } else {
                Sound::EntityPlayerAttackStrong
            };
            let sound_cat = SoundCategory::Player;

            let mut main_layer = chunk_layers.single_mut();
            main_layer.play_sound(sound_effect, sound_cat, victim_pos.0, 1.0, 1.0);
            main_layer.play_sound(Sound::EntityPlayerHurt, sound_cat, victim_pos.0, 1.0, 1.0);
            main_layer
                .view_writer(victim_pos.0)
                .write_packet(&EntityDamageS2c {
                    entity_id: VarInt(victim_id.get()),
                    source_type_id: VarInt(32),
                    source_cause_id: VarInt(attacker_id.get()),
                    source_direct_id: VarInt(attacker_id.get()),
                    source_pos: None,
                });
        }
    }
}

pub fn apply_knockback(
    mut players: Query<(&mut CombatState, Option<&mut Client>, &Position, Entity)>,
    mut sprinting: EventReader<SprintEvent>,
    mut interact_entity: EventReader<InteractEntityEvent>,
    server: Res<Server>,
) {
    for event in interact_entity.iter() {
        if let Ok(
            [(_, _, attacker_pos, attacker), (victim_state, mut victim_client, victim_pos, _)],
        ) = players.get_many_mut([event.client, event.entity])
        {
            // Victim or attacker does exist, and the attacker is not attacking itself
            if server.current_tick() - victim_state.last_attacked_tick < 10 {
                continue;
            };
            let dir = (victim_pos.0.xz() - attacker_pos.0.xz())
                .normalize()
                .as_vec2();

            let is_sprinting = sprinting
                .iter()
                .any(|event| attacker == event.client && event.state == SprintState::Start);

            let (kb_xz, kb_y) = match is_sprinting {
                true => (18.0, 8.432),
                false => (8.0, 6.432),
            };

            if let Some(client) = &mut victim_client {
                client.set_velocity([dir.x * kb_xz, kb_y, dir.y * kb_xz]);
                client.write_packet(&DamageTiltS2c {
                    entity_id: VarInt(0),
                    yaw: dir.x,
                });
            } else {
                tracing::debug!("Knockback for non players is not implemented yet.");
            }
        }
    }
}

pub fn detect_crits(mut entitys: Query<(&mut CombatState, &Position, &OldPosition), With<Client>>) {
    for (mut state, pos, old_pos) in entitys.iter_mut() {
        state.can_crit = pos.get().y < old_pos.get().y;
    }
}

pub fn apply_attack_speed(
    mut packets: EventReader<PacketEvent>,
    mut invs: Query<(&Inventory, &mut Client), With<Client>>,
) {
    for packet in packets.iter() {
        if let Some(pkt) = packet.decode::<UpdateSelectedSlotC2s>() {
            if let Ok(mut player_inv) = invs.get_mut(packet.client) {
                let inv_slot = (pkt.slot + 36) as u16;
                let item = player_inv.0.slot(inv_slot).clone();
                player_inv.1.write_packet(&EntityAttributesS2c {
                    entity_id: VarInt(0),
                    properties: vec![AttributeProperty {
                        key: Ident::new(Cow::from("generic.attack_speed")).unwrap(),
                        value: 4.0,
                        modifiers: vec![AttributeModifier {
                            uuid: Uuid::new_v4(),
                            amount: (-(4.0 - get_attack_speed(&item))).into(),
                            operation: 0,
                        }],
                    }],
                })
            }
        }
    }
}
