use super::get_weapon_cooldown_multiplier;
use super::get_weapon_stats;
use super::item_values::get_held_item;

use super::CombatQueryItem;

pub fn calc_final_damage(attacker: &CombatQueryItem, server_tick: i64) -> (f32, bool) {
    //get attack damage and attack speed of the attackers weapon
    let (weapon_dmg, attack_speed);
    if let (Some(inv), Some(held)) = (attacker.inv, attacker.held) {
        let held = get_held_item(inv, held);
        (weapon_dmg, attack_speed) = get_weapon_stats(&held);
    } else {
        (weapon_dmg, attack_speed) = (0.5, 4.0);
    }

    let weapon_cooldown_multiplier = get_weapon_cooldown_multiplier(
        server_tick,
        attacker.state.last_tick_attacking,
        attack_speed,
    );
    tracing::debug!(
        server_tick,
        attacker.state.last_tick_attacking,
        attack_speed
    );

    tracing::debug!(weapon_cooldown_multiplier);

    if attacker.state.can_crit && weapon_cooldown_multiplier > 0.99 {
        return (weapon_dmg * weapon_cooldown_multiplier * 1.5, true);
    } else {
        return (weapon_dmg * weapon_cooldown_multiplier, false);
    }
}
