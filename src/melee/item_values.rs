use valence::ItemKind::*;
use valence::{inventory::HeldItem, prelude::*};

pub fn get_held_item(inv: &Inventory, held: &HeldItem) -> ItemStack {
    inv.slot(held.slot()).clone()
}
pub fn get_weapon_stats(item: &ItemStack) -> (f32, f32) {
    (get_attack_damage(item), get_attack_speed(item))
}

fn get_nbt_value(item: &ItemStack, query: &str) -> Option<f32> {
    item.nbt.clone()?.get(query)?.clone().as_f32()
}
pub fn get_attack_damage(item: &ItemStack) -> f32 {
    if get_nbt_value(item, "attack_damage") == None {
        match item.item {
            DiamondSword | WoodenAxe | GoldenAxe => 7.0,
            IronSword => 6.0,
            GoldenSword | WoodenSword => 4.0,
            StoneSword => 5.0,
            NetheriteSword => 8.0,
            StoneAxe | IronAxe | DiamondAxe => 9.0,
            NetheriteAxe => 10.0,
            _ => 1.0,
        }
    } else {
        get_nbt_value(item, "attack_damage").unwrap_or(1.0)
    }
}
pub fn get_attack_speed(item: &ItemStack) -> f32 {
    if get_nbt_value(item, "attack_speed") == None {
        match item.item {
            DiamondSword | IronSword | GoldenSword | WoodenSword | StoneSword | NetheriteSword => {
                1.6
            }
            WoodenAxe | StoneAxe => 0.8,
            IronAxe => 0.9,
            GoldenAxe | DiamondAxe | NetheriteAxe => 1.0,
            _ => 4.0,
        }
    } else {
        get_nbt_value(item, "attack_speed").unwrap_or(4.0)
    }
}

/// how much the damage of a hit is decreased because of attack cooldown
pub fn get_weapon_cooldown_multiplier(
    curr_tick: i64,
    last_attacking_tick: i64,
    weapon_attack_speed: f32,
) -> f32 {
    let wait_ticks = weapon_attack_speed.recip() * 20.0;
    let waited_ticks = (curr_tick - last_attacking_tick) as f32;
    if waited_ticks >= wait_ticks {
        return 1.0;
    } else {
        return waited_ticks / wait_ticks;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn check_words() {
        // TODO check some swords, if they get correct value
    }

    #[test]
    fn weapon_cooldown() {
        // we attacked 2 ticks ago and our weapon can only attack once per second
        assert!(get_weapon_cooldown_multiplier(1000, 995, 1.0) == 0.25);

        // we attacked 5 ticks ago and can do 4 hits per second
        assert!(get_weapon_cooldown_multiplier(1000, 995, 4.0) == 1.0);

        // we attacked long ago so our weapon speed doesnt matter
        assert!(get_weapon_cooldown_multiplier(1000, 500, 0.1) == 1.0);
        assert!(get_weapon_cooldown_multiplier(1000, 500, 50.0) == 1.0);

        // our weapon does 1 hit every 2 seconds, but we only waited 1 second, so dmg is halfed
        assert!(get_weapon_cooldown_multiplier(1000, 980, 0.5) == 0.5);
    }
}
